//
//  CustomMessageTextField.swift
//  WoundCarePros
//
//  Created by Kawalpreet Kaur on 07/11/19.
//  Copyright © 2019 Ratnesh Swarnkar. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
class CustomMessageTextField:  SkyFloatingLabelTextField{
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.setup()
    }
    required override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    func setup(){
        self.font = UIFont.PoppinsRegular(fontSize: 14.0)
        self.textColor = UIColor.appGreyColor()
        self.rightViewMode = UITextField.ViewMode.always
        self.titleColor = UIColor.appRedColor()
        self.titleFont = UIFont.PoppinsSemiBold(fontSize: 16.0)
        self.selectedTitleColor =  UIColor.appRedColor()
        self.tintColor = UIColor.appGreyColor()
        self.setTitleVisible(true)
        self.lineColor = UIColor.appGreyColor()

    }
    
    func setErrorProperties(){
        self.lineColor = UIColor.appRedColor()
        //self.placeHolderColor = UIColor.appRedColor()
        self.lineHeight = 2.0
        self.titleColor = UIColor.appRedColor()
        
    }
    
    func setValidProperties(){
        self.lineColor = UIColor.appGreyColor()
        //self.placeHolderColor = UIColor.appGreyColor()
        self.lineHeight = 1.0
    }
    
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        var rightViewBounds = CGRect(x:0, y:0, width: 0, height: 0)
        
        if let rightView = self.rightView{
            let offset = 5
            let width  = rightView.frame.size.width
            let height = rightView.frame.size.height
            let x = CGFloat(bounds.width) - CGFloat(width) - CGFloat(offset)
            let y = CGFloat(offset)
            rightViewBounds = CGRect(x:CGFloat(x - 5.0), y:CGFloat(y + 15.0), width: width, height: height)
        }
        return rightViewBounds
    }
    
    override func becomeFirstResponder() -> Bool {
        setTitleVisible(true)
        return super.becomeFirstResponder()
    }
    
    override func resignFirstResponder() -> Bool {
        setTitleVisible(hasText || hasErrorMessage)
        return super.resignFirstResponder()
    }
    
    
}
