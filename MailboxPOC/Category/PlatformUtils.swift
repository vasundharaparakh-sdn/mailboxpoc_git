//
//  PlatformUtils.swift
//  WoundCarePros
//
//  Created by Vasundhara Parakh on 10/17/19.
//  Copyright © 2019 Ratnesh Swarnkar. All rights reserved.
//

import UIKit

struct PlatformUtils {
    static let isSimulator: Bool = {
        var isSim = false
        #if arch(i386) || arch(x86_64)
        isSim = true
        #endif
        return isSim
    }()
}
