//
//  ViewController.swift
//  MailboxPOC
//
//  Created by Vasundhara Parakh on 11/11/19.
//  Copyright © 2019 Vasundhara Parakh. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func btnNavigateToInbox(_ sender: UIButton){
        let storyboard = UIStoryboard(name: Identifiers.Storyboard.main, bundle: nil)
        let messagesAlertsViewController = storyboard.instantiateViewController(withIdentifier: Identifiers.Controller.MessagesAlertsViewController) as! MessagesAlertsViewController
        self.navigationController?.pushViewController(messagesAlertsViewController, animated: true)
    }

}

