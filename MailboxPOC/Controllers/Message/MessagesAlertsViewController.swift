//
//  MessagesAlertsViewController.swift
//  WoundCarePros
//
//  Created by Vasundhara Parakh on 9/26/19.
//  Copyright © 2019 Ratnesh Swarnkar. All rights reserved.
//

import UIKit

enum MailboxMenu : Int {
    case inbox = 0
    case sent
    case deleted
    case count
}

class MessagesAlertsViewController: BaseViewControler{
    @IBOutlet weak var tabsView: TabCollectionView!
    @IBOutlet weak var tblMails : UITableView!
    @IBOutlet weak var btnSelectAll: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    
    let mailboxMenuArray = ["Inbox", "Sent", "Deleted"]
    var mailBoxType : Int = MailboxMenu.inbox.rawValue
    var isSelectionModeOn = false
    var shouldSelectAll = false
    
    var refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.initialSetup()
        self.addRefreshController()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }
    
    func initialSetup(){
        
        Utility.setupNavigationBarWithLeftAndRightButtons(navItem: self.navigationItem, navController: self.navigationController!, title: ScreenTitles.messagesAlerts, hasleftButton: true, leftActionMethod: #selector(MessagesAlertsViewController.btnMenuClicked(sender:)), leftTarget: self, leftBtnImage: UIImage.sideMenuImage(), hasRightButton: true, rightActionMethod: #selector(MessagesAlertsViewController.btnSelectClicked(sender:)), rightTarget: self, rightBtnImage: nil,rightBtnTitle: "Select")
        
        self.tblMails.tableFooterView = nil
        self.btnDelete.isHidden = true
        
        self.updateTabView()
        
        
    }
    func addRefreshController(){
        self.refreshControl.addTarget(self, action: #selector(handleTopRefresh(_:)), for: .valueChanged )
        self.refreshControl.tintColor = UIColor.appRedColor()
        self.tblMails.addSubview(self.refreshControl)
    }
    
    @objc func handleTopRefresh(_ sender:UIRefreshControl){
        self.refreshControl.endRefreshing()
    }
    
    @objc func btnMenuClicked(sender : Any){
        
    }
    @objc func btnSelectClicked(sender : Any){
        if self.shouldSelectAll{
            return
        }
        self.isSelectionModeOn = !self.isSelectionModeOn
        self.btnDelete.isHidden = !self.isSelectionModeOn
        self.tblMails.reloadData()
    }
    
    func updateTabView(){
        self.tabsView.delegate = self
        self.tabsView.dataSourceArray.removeAll()
        self.tabsView.dataSourceArray = self.mailboxMenuArray
        self.tabsView.tabCollection.reloadData()
    }
    
    //MARK:---- IBAction----
    @IBAction func btnSelectAll_Action(_ sender: Any) {
        self.shouldSelectAll = !self.shouldSelectAll
        let boxImage = self.shouldSelectAll ? UIImage.squareCheckboxFilled() : UIImage.squareCheckboxEmpty()
        self.btnSelectAll.setImage(boxImage, for: .normal)
        self.btnDelete.isHidden = !self.shouldSelectAll
        self.isSelectionModeOn = self.shouldSelectAll
        self.tblMails.reloadData()
    }
    
    @IBAction func btnDelete_Action(_ sender: Any) {
    }
    
    @IBAction func btnComposeMail_Action(_ sender: Any) {
        let storyboard = UIStoryboard(name: Identifiers.Storyboard.main, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: Identifiers.Controller.NewMessageViewController) as! NewMessageViewController
        self.presentViewWithNavigationAnimation(vc: controller, type: .fromRight)
        
    }
    
    
}
//MARK: TableView Datasource & Delegate
extension MessagesAlertsViewController : UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.configureCell(tableView: tableView, indexPath: indexPath)
    }
    
    func configureCell(tableView : UITableView, indexPath : IndexPath) -> MailCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: Identifiers.Cell.MailCell, for: indexPath) as! MailCell
        self.prepareDatasourceForCell(tableView: tableView, indexPath: indexPath, cell: cell)
        cell.selectionStyle = .none
        return cell
        
    }
    
    func prepareDatasourceForCell(tableView : UITableView, indexPath : IndexPath, cell: MailCell){
        
        cell.btnSelectionWidthConstraint.constant =  self.isSelectionModeOn || self.shouldSelectAll ? 20 : 0
        cell.leadingConstraintForProfilePic.constant =  self.isSelectionModeOn || self.shouldSelectAll  ? 15 : 0
        
        
        cell.btnSelection.tag = indexPath.row
        cell.btnSelection.addTarget(self, action: #selector(self.cellBtnSelection_Action(_:)), for: .touchUpInside)
        
        cell.isSelected = true

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !self.isSelectionModeOn && !self.shouldSelectAll{
            self.navigateToMessageDetail()
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if self.shouldSelectAll  {
                tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        }
    }

    func navigateToMessageDetail(){
        let storyboard = UIStoryboard(name: Identifiers.Storyboard.main, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: Identifiers.Controller.MessageDetailViewController) as! MessageDetailViewController
        self.presentViewWithNavigationAnimation(vc: controller, type: .fromRight)
    }
    

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
    @IBAction func cellBtnSelection_Action(_ sender: UIButton) {
        let cell = self.tblMails.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as! MailCell
        cell.isSelected = !cell.isSelected
    }

}

//MARK:- TabCollectionViewDelegate
extension MessagesAlertsViewController : TabCollectionViewDelegate{
    func didSelectTabAtIndex(row: Int) {
        self.mailBoxType = row
        
        self.btnDelete.isHidden = true
        self.shouldSelectAll = false
        self.isSelectionModeOn = false
        
        let boxImage = self.shouldSelectAll ? UIImage.squareCheckboxFilled() : UIImage.squareCheckboxEmpty()
        self.btnSelectAll.setImage(boxImage, for: .normal)

        self.tblMails.reloadData()
    }
}

//MARK:- MailCell
class MailCell : UITableViewCell{
    
    @IBOutlet weak var leadingConstraintForProfilePic: NSLayoutConstraint!
    @IBOutlet weak var lblSubject: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgProfilePic: UIImageView!
    @IBOutlet weak var btnSelection: UIButton!
    @IBOutlet weak var btnSelectionWidthConstraint: NSLayoutConstraint!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
           super.setSelected(selected, animated: animated)

           self.btnSelection.setImage(selected ? UIImage.squareCheckboxFilled(): UIImage.squareCheckboxEmpty(), for: .normal)
    }
}
