//
//  NewMessageViewController.swift
//  WoundCarePros
//
//  Created by Kawalpreet Kaur on 07/11/19.
//  Copyright © 2019 Ratnesh Swarnkar. All rights reserved.
//

import UIKit
import MobileCoreServices
import OpalImagePicker
import TagListView
import PDFKit
import Photos
class NewMessageViewController: BaseViewControler {
    let maxAttachmentCount = 5
    
    @IBOutlet weak var tagListView: TagListView!
    @IBOutlet weak var attachmentCollectionView: UICollectionView!

    var attachmentArray = [Any]()
    var preSelectedReciepients = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.initialSetup()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }
    
    func initialSetup(){

        self.setupTagList()
    }
    
    func setupTagList(){
        self.tagListView.textFont = UIFont.PoppinsSemiBold(fontSize: 14)
        self.tagListView.alignment = .left // possible values are .left, .center, and .right
        self.tagListView.delegate = self
    }
    //MARK:-----IBActions----
    @IBAction func actionCancel(_ sender: UIButton){
        self.dismissViewWithNavigationAnimation()
    }
    @IBAction func actionAttachment(_ sender: UIButton){
        
        if self.attachmentArray.count < 5 {
            // create an actionSheet
        let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        // create an action
        let firstAction: UIAlertAction = UIAlertAction(title: StringLiterals.attachMedia, style: .default) { action -> Void in
            self.openImagePicker()
        }
        let secondAction: UIAlertAction = UIAlertAction(title: StringLiterals.attachDoc, style: .default) { action -> Void in
            self.openDocumentPicker()
        }
        let cancelAction: UIAlertAction = UIAlertAction(title: StringLiterals.cancelString, style: .destructive) { action -> Void in }
        // add actions
        actionSheetController.addAction(firstAction)
        actionSheetController.addAction(secondAction)
        actionSheetController.addAction(cancelAction )
        actionSheetController.view.tintColor = .appDarkGreyColor()
        actionSheetController.popoverPresentationController?.sourceView = self.view // works for both iPhone & iPad
        present(actionSheetController, animated: true) {
            //print("option menu presented")
        }
        }else{
            self.showAlert(with: Alert.Message.maxAttachmentLimitReached)
        }
    }
    
    func openImagePicker(){
        let imagePicker = OpalImagePickerController()
        //Limit maximum allowed selections to 5
        imagePicker.maximumSelectionsAllowed = maxAttachmentCount - self.attachmentArray.count
        //Only allow image media type assets
        //imagePicker.allowedMediaTypes = Set([PHAssetMediaType.image])
        //Change default localized strings displayed to the user
        let configuration = OpalImagePickerConfiguration()
        configuration.maximumSelectionsAllowedMessage = Alert.Message.maxAttachmentLimitReached
        imagePicker.configuration = configuration

        presentOpalImagePickerController(imagePicker, animated: true,
            select: { (assets) in
                //Select Assets

                imagePicker.dismiss(animated: true) {
                    self.attachmentArray.append(contentsOf: assets)
                    self.attachmentCollectionView.reloadData()

                }
            }, cancel: {
                //Cancel
                imagePicker.dismiss(animated: true, completion: nil)
            })
    }
    func openDocumentPicker(){
        let documentPickerController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF), String(kUTTypeImage)], in: .import)
        documentPickerController.delegate = self
        self.present(documentPickerController, animated: true, completion: nil)

    }
    
    
    
    @IBAction func actionSend(_ sender: UIButton){
        
    }
    
    @IBAction func actionAddReciever(_ sender: UIButton){
        self.view.endEditing(true)
        self.navigateToRecieverList()
    }

    func navigateToRecieverList(){
        let storyboard = UIStoryboard(name: Identifiers.Storyboard.main, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: Identifiers.Controller.MailRecieverListViewController) as! MailRecieverListViewController
        controller.delegate = self
        controller.selectedRecievers = self.preSelectedReciepients
        self.presentViewWithNavigationAnimation(vc: controller, type: .fromRight)
    }

}
//MARK:- UITextFieldDelegate
extension NewMessageViewController : UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
        self.navigateToRecieverList()
    }
}
//MARK:- UICollectionViewDelegate & UICollectionViewDataSource
extension NewMessageViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return attachmentArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Identifiers.Cell.MessageAttachmentCell, for: indexPath) as! MessageAttachmentCell
        
//        if let url = self.attachmentArray[indexPath.row] as? URL{
//            cell.imgAttachment.image = self.pdfThumbnail(withUrl: url)
//        }
       
        //Attachment Type = Photo
        if let asset = self.attachmentArray[indexPath.row] as? PHAsset{
            asset.image(completionHandler: { (thumbnail) in
                cell.imgAttachment.image = thumbnail
            })
        }
        
        //Attachment Type = PDF
        if let _ = self.attachmentArray[indexPath.row] as? URL{
            cell.imgAttachment.image = UIImage.pdfPlaceholder()
        }
        
        cell.btnDelete.tag = indexPath.row
        
        cell.btnDelete.addTarget(self, action: #selector(self.btnDeleteAttachment_Action(_:)), for: .touchUpInside)

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
         
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return  CGSize(width: 80, height: 80)
    }
    
    @objc func btnDeleteAttachment_Action(_ sender: UIButton){
        self.attachmentArray.remove(at: sender.tag)
        self.attachmentCollectionView.reloadData()
    }


    
}
//MARK:- UIDocumentMenuDelegate, UIDocumentPickerDelegate
extension NewMessageViewController : UIDocumentMenuDelegate, UIDocumentPickerDelegate,UINavigationControllerDelegate {

    func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        self.present(documentPicker, animated: true, completion: nil)
    }

    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        self.attachmentArray.append(url)
        self.attachmentCollectionView.reloadData()
        print("url = \(url)")
    }

    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}
//MARK:- TagListViewDelegate
extension NewMessageViewController : TagListViewDelegate{
    func tagPressed(title: String, tagView: TagView, sender: TagListView) {
        print("Tag pressed: \(title), \(sender)")
    }
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print("Tag Remove pressed: \(title), \(sender)")
        sender.removeTagView(tagView)
        if let indexToDelete = self.preSelectedReciepients.index(of: title){
            self.preSelectedReciepients.remove(at: indexToDelete)
        }
    }
}

//MARK:- PDF Thumbnail
extension NewMessageViewController {
    
    func pdfThumbnail(withUrl url:URL, pageNumber:Int = 1, width: CGFloat = 240) -> UIImage? {
        guard let pdf = CGPDFDocument(url as CFURL),
            let page = pdf.page(at: pageNumber)
            else {
                return nil
        }

        var pageRect = page.getBoxRect(.mediaBox)
        let pdfScale = width / pageRect.size.width
        pageRect.size = CGSize(width: pageRect.size.width*pdfScale, height: pageRect.size.height*pdfScale)
        pageRect.origin = .zero

        UIGraphicsBeginImageContext(pageRect.size)
        let context = UIGraphicsGetCurrentContext()!

        // White BG
        context.setFillColor(UIColor.white.cgColor)
        context.fill(pageRect)
        context.saveGState()

        // Next 3 lines makes the rotations so that the page look in the right direction
        context.translateBy(x: 0.0, y: pageRect.size.height)
        context.scaleBy(x: 1.0, y: -1.0)
        context.concatenate(page.getDrawingTransform(.mediaBox, rect: pageRect, rotate: 0, preserveAspectRatio: true))

        context.drawPDFPage(page)
        context.restoreGState()

        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    /*
    func pdfThumbnail(url: URL, width: CGFloat = 240) -> UIImage? {
      guard let data = try? Data(contentsOf: url),
      let page = PDFDocument(data: data)?.page(at: 0) else {
        return nil
      }

      let pageSize = page.bounds(for: .mediaBox)
      let pdfScale = width / pageSize.width

      // Apply if you're displaying the thumbnail on screen
      let scale = UIScreen.main.scale * pdfScale
      let screenSize = CGSize(width: pageSize.width * scale,
                              height: pageSize.height * scale)

      return page.thumbnail(of: screenSize, for: .mediaBox)
    }*/
}

extension NewMessageViewController : MailRecieverListDelegate{
    func getSelectedRecipients(nameArray: [String]) {
        self.tagListView.removeAllTags()
        for (_,item) in nameArray.enumerated(){
            self.tagListView.addTag(item)
        }
        self.preSelectedReciepients = nameArray
    }
}

//MARK:- MessageAttachmentCell
class MessageAttachmentCell : UICollectionViewCell{
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var imgAttachment: UIImageView!
}

extension PHAsset {

    func image(completionHandler: @escaping (UIImage) -> ()){
        var thumbnail = UIImage()
        let imageManager = PHCachingImageManager()
        imageManager.requestImage(for: self, targetSize: CGSize(width: 100, height: 100), contentMode: .aspectFit, options: nil, resultHandler: { img, _ in
            thumbnail = img!
        })
        completionHandler(thumbnail)
    }
    
    var data : (UIImage, [AnyHashable : Any]) {
        var img = UIImage(); var information = [AnyHashable : Any](); let imageManager = PHCachingImageManager()
        imageManager.requestImage(for: self, targetSize: CGSize(width: 100, height: 100), contentMode: .aspectFit, options: nil, resultHandler: { image,info in
            img = image!
            information = info!
        })
        return (img,information)
    }

}
