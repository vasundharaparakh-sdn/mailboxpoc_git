//
//  MailRecieverListViewController.swift
//  WoundCarePros
//
//  Created by Vasundhara Parakh on 11/7/19.
//  Copyright © 2019 Ratnesh Swarnkar. All rights reserved.
//

import UIKit
protocol MailRecieverListDelegate {
    func getSelectedRecipients(nameArray : [String])
}

class MailRecieverListViewController: BaseViewControler {
    
    var delegate: MailRecieverListDelegate?
    @IBOutlet weak var tblRecieverList : UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    var arrForReciever = [String](){
        didSet{
            if self.arrForReciever.count > 0{
                Utility.setEmptyTableFooter(tableView: self.tblRecieverList)
            }else{
                Utility.setTableFooterWithMessage(tableView: self.tblRecieverList, message: Alert.Message.noReciepients)
            }
        }
    }
    var filteredRecievers = [String](){
        didSet{
            if self.filteredRecievers.count > 0{
                Utility.setEmptyTableFooter(tableView: self.tblRecieverList)
            }else{
                Utility.setTableFooterWithMessage(tableView: self.tblRecieverList, message: Alert.Message.noReciepients)
            }
        }
    }
    
    var selectedRecievers = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.initialSetup()
    }
    
    func initialSetup(){

        let apiResponse = ["Dan Brown","Ricky Shaw","Emma Louis","Renne Louis","Jeff Stones", "Robert Stanley"]
        self.arrForReciever = apiResponse
        self.tblRecieverList.tableFooterView = nil
        
        self.tblRecieverList.reloadData()

    }
    //MARK:-----IBActions----
    @IBAction func btnBack_action(_ sender: UIButton){
        self.dismissViewWithNavigationAnimation()
    }
    @IBAction func btnDone_action(_ sender: UIButton){
        if !self.selectedRecievers.isEmpty{
            delegate?.getSelectedRecipients(nameArray: self.selectedRecievers)
            self.selectedRecievers.removeAll()
            self.dismissViewWithNavigationAnimation()
        }
    }

}
//MARK: TableView Datasource & Delegate
extension MailRecieverListViewController : UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filteredRecievers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.configureCell(tableView: tableView, indexPath: indexPath)
    }
    
    func configureCell(tableView : UITableView, indexPath : IndexPath) -> RecieverCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: Identifiers.Cell.RecieverCell, for: indexPath) as! RecieverCell
        self.prepareDatasourceForCell(tableView: tableView, indexPath: indexPath, cell: cell)
        cell.selectionStyle = .none
        return cell
        
    }
    
    func prepareDatasourceForCell(tableView : UITableView, indexPath : IndexPath, cell: RecieverCell){
        let reciever = self.filteredRecievers[indexPath.row]
        cell.lblName.text = reciever
        
        cell.imgSelection.isHidden = !self.selectedRecievers.contains(reciever)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         let reciever = self.filteredRecievers[indexPath.row]
        let cell = tableView.cellForRow(at: indexPath) as! RecieverCell
        
        if self.selectedRecievers.contains(reciever){
            if let indexToRemove = self.selectedRecievers.index(of: reciever){
                self.selectedRecievers.remove(at: indexToRemove)
            }
        }else{
            self.selectedRecievers.append(reciever)
        }
        
        cell.imgSelection.isHidden = !self.selectedRecievers.contains(reciever)

    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
}

//MARK:------UISearchBarDelegate------
extension MailRecieverListViewController: UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        self.filteredRecievers = searchText.isEmpty ? self.arrForReciever : self.arrForReciever.filter({($0.lowercased().contains(searchText.lowercased()))})
        self.tblRecieverList.reloadData()
        
    }
    
    
}

//MARK:- API calling
extension MailRecieverListViewController{
    func getRecieverList(){
        
    }
}

//MARK:- RecieverCell
class RecieverCell : UITableViewCell{
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgSelection: UIImageView!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
           super.setSelected(selected, animated: animated)
           //self.imgSelection.isHidden = !selected
    }

}
