//
//  MessageDetailViewController.swift
//  WoundCarePros
//
//  Created by Vasundhara Parakh on 11/7/19.
//  Copyright © 2019 Ratnesh Swarnkar. All rights reserved.
//

import UIKit

class MessageDetailViewController: BaseViewControler {
    @IBOutlet weak var detailViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewDetailInfo: UIView!
    @IBOutlet weak var lblDetailFrom: UILabel!
    @IBOutlet weak var lblDetailTo: UILabel!
    @IBOutlet weak var lblDetailDate: UILabel!
    @IBOutlet weak var lblSubject: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var imgProfilePic: UIImageView!
    @IBOutlet weak var textViewMessageBody: UITextView!
    @IBOutlet weak var collectionAttachments: UICollectionView!

    var shouldShowDetail = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(animated)
        self.initialSetup()
    }
    
    func initialSetup(){
        self.detailViewHeightConstraint.constant = 0.0
        self.viewDetailInfo.isHidden = !self.shouldShowDetail
        
        self.setupDetailView()
    }
    
    func setupDetailView(){
        //From
        let prefixFrom = "From : "
        let strFrom = prefixFrom + "WCC"
        self.lblDetailFrom.attributedText = Utility.highlightPartialTextOfLabel(mainString: strFrom, highlightString: prefixFrom,highlightColor: UIColor.appDarkGreyColor(),highlightFont:UIFont.PoppinsSemiBold(fontSize: 14.0) )
        
        //To
        let prefixTo = "To : "
        let strTo = prefixTo + "Whomsoever it may concern"

        self.lblDetailTo.attributedText = Utility.highlightPartialTextOfLabel(mainString: strTo, highlightString: prefixTo,highlightColor: UIColor.appDarkGreyColor(),highlightFont:UIFont.PoppinsSemiBold(fontSize: 14.0) )

        //Date
        let prefixDate = "Date : "
        let strDate = prefixDate + "12/12/2019"
        self.lblDetailDate.attributedText = Utility.highlightPartialTextOfLabel(mainString: strDate, highlightString: prefixDate,highlightColor: UIColor.appDarkGreyColor(),highlightFont:UIFont.PoppinsSemiBold(fontSize: 14.0) )

    }
    //MARK:-----IBActions----
    @IBAction func btnBack_action(_ sender: UIButton){
        self.dismissViewWithNavigationAnimation()
    }
    @IBAction func btnReply_action(_ sender: UIButton){
        
    }
    @IBAction func btnForward_action(_ sender: UIButton){
        
    }
    @IBAction func btnDelete_action(_ sender: UIButton){
        
    }

    @IBAction func btnShowDetail_action(_ sender: UIButton){
        self.shouldShowDetail = !self.shouldShowDetail
        
        self.viewDetailInfo.isHidden = !self.shouldShowDetail
        UIView.animate(withDuration: 2, animations: {
        }) { (finished) in
            self.detailViewHeightConstraint.constant = self.shouldShowDetail ? 75.0: 0.0
        }

    }
}
//MARK:- UICollectionViewDelegate & UICollectionViewDataSource
extension MessageDetailViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Identifiers.Cell.MessageAttachmentCell, for: indexPath) as! MessageAttachmentCell
        
        cell.btnDelete.isHidden = true
       
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.showAttachmentWithURL(url: "")
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return  CGSize(width: 80, height: 80)
    }
    
    func showAttachmentWithURL(url : String){
        //TODO: For Image -> open image preview screen.
        //TODO: for PDF -> open pdf viewer.
    }

}
