//
//  BaseViewControler.swift
//  iOSArchitecture
//
//  Created by Amit on 24/02/18.
//  Copyright © 2018 smartData. All rights reserved.
//

import Foundation
import UIKit

typealias CompletionHandler = (_ success:Bool) -> Void

class BaseViewControler: UIViewController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.appScreenBgColor()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showLoader() {
        // UIApplication.shared.isNetworkActivityIndicatorVisible = true
        // SwiftLoader.show(true)
        //startAnimating(CGSize(width: 50.0, height: 50.0), message: "", messageFont: nil, type: self.animationType, color:UIColor.appRedColor() , padding: 0.0, displayTimeThreshold: 0, minimumDisplayTime: nil, backgroundColor: UIColor.lightText)
    }
    
    func hideLoader() {
      //stopAnimating()
    }
    
    func showAlert(with message:String) {
            AlertManager.showOKAlert(withTitle: Alert.Title.appName, withMessage: message, onViewController: self).view.tintColor = UIColor.appRedColor()
    }

    
}

