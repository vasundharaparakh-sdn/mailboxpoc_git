//
//  Utility.swift
//  WoundCarePros
//
//  Created by Vasundhara Parakh on 7/02/19.
//  Copyright © 2019 Ratnesh Swarnkar. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration
import MobileCoreServices
import AVFoundation
import AVKit
import LocalAuthentication

public protocol NibLoadable {
    static var nibName: String { get }
}

public extension NibLoadable where Self: UIView {
    
    public static var nibName: String {
        return String(describing: Self.self) // defaults to the name of the class implementing this protocol.
    }
    
    public static var nib: UINib {
        let bundle = Bundle(for: Self.self)
        return UINib(nibName: Self.nibName, bundle: bundle)
    }
    
    func setupFromNib() {
        guard let view = Self.nib.instantiate(withOwner: self, options: nil).first as? UIView else { fatalError("Error loading \(self) from nib") }
        addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leadingAnchor, constant: 0).isActive = true
        view.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        view.trailingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.trailingAnchor, constant: 0).isActive = true
        view.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
    }
}
struct Platform {
    
    static var isSimulator: Bool {
        return TARGET_OS_SIMULATOR != 0
    }
    
}
class Utility {
    
    var isIphoneXOrBigger: Bool {
        // 812.0 on iPhone X, XS.
        // 896.0 on iPhone XS Max, XR.
        return UIScreen.main.bounds.height >= 812
    }

    class var canUseFaceID: Bool {
        
        if #available(iOS 11.0, *) {
            let context = LAContext()
            return (context.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: nil) && context.biometryType == LABiometryType.faceID)
        }
        return false

    }
    
    // MARK: - Get Image With Color
    class func imageWithColor(_ color: UIColor, size:CGSize) -> UIImage? {
        let rect = CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height)
        UIGraphicsBeginImageContext(rect.size)
        if let context = UIGraphicsGetCurrentContext(){
            context.setFillColor(color.cgColor)
            context.fill(rect)
            
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            return image!
        }
        return nil
    }
        
    // MARK: - Network Check Method
    class func isNetworkAvailable() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    
    
    class func getTrimmedStringWithoutExtraSpacesAndNewLineCharacter(_ rawString : String) -> String {
        return rawString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    
    //MARK: getHrsFromMinutes
    class func getHrsFromMinutes(minutes : Int) -> String{
        var time = ""
        let hr = minutes/60
        let minutes = minutes % 60
        var formatHr = "hr"
        if hr > 1{
            formatHr = "hrs"
        }
        var formatMin = "min"
        if minutes > 1{
            formatMin = "mins"
        }

        //print("Session Time Duraction ==== \("\(hr) \(formatHr) \(minutes) \(formatMin)")")
        time = "\(hr) \(formatHr) \(minutes) \(formatMin)"
        if minutes == 0 {
            //print("Session Time Duraction ==== \("\(hr) \(formatHr)")")
            time = "\(hr) \(formatHr)"
        }
        if hr == 0 {
            time = "\(minutes) \(formatMin)"
        }
        return time
    }
    
    //MARK: getHrsFromMinutes
    class func getHrsFromSeconds(seconds : Int) -> String{
        var time = ""
        let hr = seconds/60/60
        let minutes = seconds / 60
        let seconds = seconds % 60
        var formatHr = "hr"
        if hr > 1{
            formatHr = "hrs"
        }
        var formatMin = "min"
        if minutes > 1{
            formatMin = "mins"
        }
        
        var formatSeconds = "sec"
        if seconds > 1 {
            formatSeconds = "secs"
        }
        //print("Session Time Duraction ==== \("\(hr) \(formatHr) \(minutes) \(formatMin)")")
        //time = "\(hr) \(formatHr) \(minutes) \(formatMin) \(seconds) \(formatSeconds)"
        
        if hr != 0 {
            time.append("\(hr) \(formatHr) ")
        }
        
        if minutes != 0 {
            //print("Session Time Duraction ==== \("\(hr) \(formatHr)")")
            time.append("\(minutes) \(formatMin) ")
        }
        
        if seconds != 0 {
            time.append("\(seconds) \(formatSeconds)")
        }

        if time.count == 0 {
            return "0"
        }
        return time
    }

    
    //MARK: getHrsFromMinutes
    class func getTimeStringFromSeconds(seconds : Int64) -> String{
        let hr = seconds / 3600
        let minutes = seconds / 60 % 60
        let seconds = seconds % 60
        var time = ""

        var formatHr = "hr"
        if hr > 1{
            formatHr = "hrs"
        }
        var formatMin = "min"
        if minutes > 1{
            formatMin = "mins"
        }
        
        var formatSec = "sec"
        if seconds > 1{
            formatSec = "secs"
        }
        
        //print("Session Time Duraction ==== \("\(hr)\(formatHr) \(minutes)\(formatMin)")")
        if hr > 0{
            time.append("\(hr) \(formatHr)")
        }
        
        time.append(" \(minutes) \(formatMin)")
        
        if seconds > 0{
            time.append(" \(seconds) \(formatSec)")
        }
        
        
        return time
    }

    //MARK:- TimeStamp Time ago Date Format
    class func setTimeAgoStringFromTimeStamp(_ timeStamp: Double , dateFormat : String) -> String? {
        let date = Date(timeIntervalSince1970: timeStamp/1000)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        return dateFormatter.string(from: date)
    }
    
    
    class func addPercentEncoding(urlString:String) -> String?
    {
      var decodedStr = urlString.trimmingCharacters(in: .whitespacesAndNewlines).removingPercentEncoding
        
      decodedStr = decodedStr?.trimmingCharacters(in: .whitespacesAndNewlines).addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)//urlPathAllowed
        return decodedStr
    }
    
    //MARK:- setupNavigationBar
    class func setupNavigationBar(navItem:UINavigationItem, navController : UINavigationController, title: String , hasMenuButton : Bool, leftActionMethod: Selector?,leftTarget : AnyObject?){
        
        //Customize Title
        navController.navigationBar.topItem?.title = title
        navController.navigationBar.titleTextAttributes =
            [NSAttributedString.Key.foregroundColor: UIColor.navigationTitleColor(),
             NSAttributedString.Key.font: UIFont.navigationTitleFont()]
        
        if hasMenuButton {
            //Add left button
            let btnLeft = UIButton(type: .custom)
            btnLeft.setImage(UIImage.sideMenuImage(), for: .normal)
            btnLeft.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
            btnLeft.addTarget(leftTarget, action: leftActionMethod!, for: .touchUpInside)
            let itemLeft = UIBarButtonItem(customView: btnLeft)
            navItem.setLeftBarButton(itemLeft, animated: true)
        }
    }

    //MARK:- setupNavigationBarWithLeftAndRightButtons
    class func setupNavigationBarWithLeftAndRightButtons(navItem:UINavigationItem, navController : UINavigationController, title: String , hasleftButton : Bool, leftActionMethod: Selector?,leftTarget : AnyObject?,leftBtnImage : UIImage?, hasRightButton : Bool, rightActionMethod: Selector?,rightTarget : AnyObject?,rightBtnImage : UIImage?,rightBtnTitle : String? ){
        
        //Customize Title
        navController.navigationItem.title = title
        navController.navigationBar.topItem?.title = title
        navController.navigationBar.titleTextAttributes =
            [NSAttributedString.Key.foregroundColor: UIColor.navigationTitleColor(),
             NSAttributedString.Key.font: UIFont.navigationTitleFont()]
        
        if hasleftButton {
            //Add left button
            let btnLeft = UIButton(type: .custom)
            btnLeft.setImage(leftBtnImage, for: .normal)
            btnLeft.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
            btnLeft.addTarget(leftTarget, action: leftActionMethod!, for: .touchUpInside)
            let itemLeft = UIBarButtonItem(customView: btnLeft)
            navItem.setLeftBarButton(itemLeft, animated: true)
        }
        
        if hasRightButton {
            //Add right button
            let btnRight = UIButton(type: .custom)
            if rightBtnImage == nil{
                btnRight.setTitle(rightBtnTitle, for: .normal)
            }else{
                btnRight.setImage(rightBtnImage, for: .normal)
            }
            btnRight.setTitleColor(UIColor.appRedColor(), for: .normal)
            btnRight.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
            btnRight.addTarget(rightTarget, action: rightActionMethod!, for: .touchUpInside)
            let itemRight = UIBarButtonItem(customView: btnRight)
            navItem.setRightBarButton(itemRight, animated: true)
        }

    }


    //MARK: highlightPartialTextOfLabel
    class func highlightPartialTextOfLabel(mainString : String, highlightString : String, highlightColor : UIColor, highlightFont : UIFont) -> NSMutableAttributedString{
        let main_string = mainString
        let string_to_color = highlightString
        let range = (main_string as NSString).range(of: string_to_color)
        let attributedString = NSMutableAttributedString(string:main_string)
        attributedString.addAttribute(NSAttributedString.Key.font, value: highlightFont , range: range)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: highlightColor , range: range)
        return attributedString
    }
    
    //MARK:- setTableFooterWithMessage
    class func setTableFooterWithMessage(tableView : UITableView,message : String){
        tableView.tableFooterView = UIView()
        tableView.tableFooterView?.frame(forAlignmentRect: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
        
        
        let emptyView  = UIView(frame: CGRect(x: 0, y: (tableView.bounds.size.height/2)-75, width: tableView.bounds.size.width, height: 150))
        
        let noResultView = Bundle.main.loadNibNamed("NoResultView", owner: nil, options: nil)?[0] as! NoResultView
        
        // noResultView.frame.origin = emptyView.frame.origin;
        noResultView.frame = CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 150)
        noResultView.backgroundColor = UIColor.clear
        
        
        noResultView.messageLabel.text = message
        emptyView.addSubview(noResultView)
        tableView.tableFooterView?.addSubview(emptyView)
    }
    
    //MARK: setEmptyTableFooter
    class func setEmptyTableFooter(tableView : UITableView){
        tableView.tableFooterView = UIView()
        tableView.tableFooterView?.frame(forAlignmentRect: CGRect(x: 0, y: 0, width: 0, height: 0))
    }
    
    
}
