//
//  Constants.swift
//  iOSArchitecture
//
//  Created by Amit on 23/02/18.
//  Copyright © 2018 smartData. All rights reserved.
//




import Foundation
import UIKit

//MARK:- AppDetails
struct AppDetails {
    static let applictionName = NSLocalizedString("Your App Name", comment: "")
}

//MARK:- Identifiers
enum Identifiers {
    enum Storyboard {
        static let main = "Main"
    }
    
    enum Controller {
        static let MessagesAlertsViewController = "MessagesAlertsViewController"
        static let NewMessageViewController = "NewMessageViewController"
        static let MessageDetailViewController = "MessageDetailViewController"
        static let MailRecieverListViewController = "MailRecieverListViewController"
    }
    
    enum Cell {
        static let MailCell = "MailCell"
        static let RecieverCell = "RecieverCell"
        static let MessageAttachmentCell = "MessageAttachmentCell"
        static let TabCollectionCell = "TabCollectionCell"
    }
    
}
//MARK:- ScreenTitles
struct ScreenTitles {
    static let messagesAlerts = NSLocalizedString("Mailbox", comment: "")
}

//MARK:- StringLiterals
struct StringLiterals {
    static let cancelString =  NSLocalizedString("Cancel", comment: "")
    static let attachMedia = NSLocalizedString("Attach Photo/Video", comment: "")
    static let attachDoc = NSLocalizedString("Attach document from iCloud", comment: "")
}

//MARK:- Alert
struct Alert{
    struct Title {
        static let appName =  NSLocalizedString("App Name", comment: "")
    }
    
    struct Message {
        static let maxAttachmentLimitReached = "Please delete few attachments to add more. Maximum limit is 5."
        static let noReciepients = NSLocalizedString("No recipients found.", comment: "noPatients")
    }
    
}

//MARK:- Macros
struct Macros
{
    static let Camera :String                   = "Camera"
    static let PHOTO_LIBRARY                    = "Photo Library"
    static let CANCEL                           = "Cancel"
    static let CHOOSE_OPTIONS                   = "Choose Option"
}
    
