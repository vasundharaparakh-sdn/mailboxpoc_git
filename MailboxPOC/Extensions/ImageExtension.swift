//
//  ImageExtension.swift
//  WoundCarePros
//
//  Created by Vasundhara Parakh on 7/2/19.
//  Copyright © 2019 Ratnesh Swarnkar. All rights reserved.
//

import UIKit

extension UIImage{
    
    //MARK:- Navigation buttons
    class func backImage() -> UIImage{
        return UIImage(named: "icons-back")!
    }
    
    class func infoImage() -> UIImage{
        return UIImage(named: "icon_info")!
    }
    class func editImage() -> UIImage{
        return UIImage(named: "edit_icon")!
    }
    
    //MARK:- TabBar Images
    class func tabbarAppointmentImage() -> UIImage{
        return UIImage(named: "calendar")!
    }
    
    class func tabbarSettingImage() -> UIImage{
        return UIImage(named: "list")!
    }
    
    class func tabbarNotificationImage() -> UIImage{
        return UIImage(named: "notification")!
    }
    
    class func tabbarProfileImage() -> UIImage{
        return UIImage(named: "user")!
    }
    
    class func tabbarAppointmentSelectedImage() -> UIImage{
        return UIImage(named: "schedule_sel")!
    }
    
    class func tabbarSettingSelectedImage() -> UIImage{
        return UIImage(named: "encounters_select")!
    }
    
    class func tabbarNotificationSelectedImage() -> UIImage{
        return UIImage(named: "notification_sel")!
    }
    
    class func tabbarProfileSelectedImage() -> UIImage{
        return UIImage(named: "profile_sel")!
    }
    
    
    //MARK:- Menu Images
    class func sideMenuImage() -> UIImage{
        return UIImage(named: "sidemenu")!
    }
    
    //MARK:- Textfield Images
    class func emailTfImage() -> UIImage{
        return UIImage(named: "email_tf")!
    }
    
    class func passwordTfImage() -> UIImage{
        return UIImage(named: "password_tf")!
    }
    
    class func dropdownTextfieldRightImage() -> UIImage{
        return UIImage(named: "redDot")!
    }
    
    class func DateTextfieldRightImage() -> UIImage{
        return UIImage(named: "tf_calendar")!
    }
    
    //MARK:- Radio button
    class func check() -> UIImage{
        return UIImage(named: "check")!
    }
    class func uncheck() -> UIImage{
        return UIImage(named: "uncheck")!
    }
    
    //MARK:- Common
    class func defaultAnatomy() -> UIImage{
        return UIImage(named: "body")!
    }
    
    class func mediaPlaceholder() -> UIImage{
        return UIImage(named: "media_placeholder")!
    }
    
    class func defaultProfilePicture() -> UIImage{
        return UIImage(named: "defaultProfilePicture")!
    }
    class func cameraActive() -> UIImage{
        return UIImage(named: "photo-camera-1")!
    }
    class func cameraInactive() -> UIImage{
        return UIImage(named: "photo-camera")!
    }
    class func fingerprint() -> UIImage{
        return UIImage(named: "fingerprint")!
    }
    class func faceRecognition() -> UIImage{
        return UIImage(named: "face-recognition")!
    }
    
    class func imgNext() -> UIImage{
        return UIImage(named:"right-arrow")!
    }
    
    //MARK:- Message Module
    class func squareCheckboxFilled() -> UIImage{
        return UIImage(named:"filled-checkbox")!
    }

    class func squareCheckboxEmpty() -> UIImage{
        return UIImage(named:"empty-checkbox")!
    }

    class func pdfPlaceholder() -> UIImage{
        return UIImage(named:"pdfPlaceholder")!
    }

    
    //MARK:- Functions
    func fixImageOrientation() -> UIImage? {
        var flip:Bool = false //used to see if the image is mirrored
        var isRotatedBy90:Bool = false // used to check whether aspect ratio is to be changed or not
        
        var transform = CGAffineTransform.identity
        
        //check current orientation of original image
        switch self.imageOrientation {
        case .down, .downMirrored:
            transform = transform.rotated(by: CGFloat(M_PI));
            
        case .left, .leftMirrored:
            transform = transform.rotated(by: CGFloat(M_PI_2));
            isRotatedBy90 = true
        case .right, .rightMirrored:
            transform = transform.rotated(by: CGFloat(-M_PI_2));
            isRotatedBy90 = true
        case .up, .upMirrored:
            break
        }
        
        switch self.imageOrientation {
            
        case .upMirrored, .downMirrored:
            transform = transform.translatedBy(x: self.size.width, y: 0)
            flip = true
            
        case .leftMirrored, .rightMirrored:
            transform = transform.translatedBy(x: self.size.height, y: 0)
            flip = true
        default:
            break;
        }
        
        // calculate the size of the rotated view's containing box for our drawing space
        let rotatedViewBox = UIView(frame: CGRect(origin: CGPoint(x:0, y:0), size: size))
        rotatedViewBox.transform = transform
        let rotatedSize = rotatedViewBox.frame.size
        
        // Create the bitmap context
        UIGraphicsBeginImageContext(rotatedSize)
        let bitmap = UIGraphicsGetCurrentContext()
        
        // Move the origin to the middle of the image so we will rotate and scale around the center.
        bitmap!.translateBy(x: rotatedSize.width / 2.0, y: rotatedSize.height / 2.0);
        
        // Now, draw the rotated/scaled image into the context
        var yFlip: CGFloat
        
        if(flip){
            yFlip = CGFloat(-1.0)
        } else {
            yFlip = CGFloat(1.0)
        }
        
        bitmap!.scaleBy(x: yFlip, y: -1.0)
        
        //check if we have to fix the aspect ratio
        if isRotatedBy90 {
            bitmap?.draw(self.cgImage!, in: CGRect(x: -size.width / 2, y: -size.height / 2, width: size.height,height: size.width))
        } else {
            bitmap?.draw(self.cgImage!, in: CGRect(x: -size.width / 2, y: -size.height / 2, width: size.width,height: size.height))
        }
        
        let fixedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return fixedImage
    }
    
}
