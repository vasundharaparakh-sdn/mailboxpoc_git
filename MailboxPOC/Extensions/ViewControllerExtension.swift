//
//  ViewControllerExtension.swift
//  WoundCarePros
//
//  Created by Vasundhara Parakh on 7/5/19.
//  Copyright © 2019 Ratnesh Swarnkar. All rights reserved.
//

import UIKit

extension UIViewController {
    func presentViewWithNavigationAnimation(vc: UIViewController, type: CATransitionSubtype) {
        let customVcTransition = vc
        let transition = CATransition()
        transition.duration = 0.4
        transition.type = CATransitionType.push
        transition.subtype = type
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        vc.modalPresentationStyle = .fullScreen
        present(customVcTransition, animated: false, completion: nil)
    }
    
    func dismissViewWithNavigationAnimation(){
        let transition: CATransition = CATransition()
        transition.duration = 0.4
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.reveal
        transition.subtype = CATransitionSubtype.fromLeft
        self.view.window!.layer.add(transition, forKey: nil)
        self.dismiss(animated: false, completion: nil)
    }
    
    func dismissViewWithNavigationAnimationWithCompletion(completion: (() -> Void)? = nil){
        let transition: CATransition = CATransition()
        transition.duration = 0.4
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.reveal
        transition.subtype = CATransitionSubtype.fromLeft
        self.view.window!.layer.add(transition, forKey: nil)
        self.dismiss(animated: false,completion: completion)
    }
}
