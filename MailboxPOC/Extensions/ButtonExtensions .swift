//
//  ButtonExtensions.swift
//  Carescribr
//
//
//  Created by Ratnesh Swarkar on 12/26/18.
//  Copyright © 2018 Ratnesh Swarkar. All rights reserved.
//

import UIKit
import TransitionButton

//@IBDesignable
class ButtonExtensions: UIButton {
    var data:AnyObject?
    
  
    func setData(data:AnyObject) {
        self.data = data
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.layer.cornerRadius = 5.0;
        self.layer.masksToBounds = true
       // self.setBackgroundImage(UIImage.navigationBarImage(), for: .highlighted)
        self.tintColor = UIColor.white
        
    }
    
}

@IBDesignable
class UISwitchCustom: UISwitch {
    @IBInspectable var OffTint: UIColor? {
        didSet {
            self.tintColor = OffTint
            self.layer.cornerRadius = 16
            self.backgroundColor = OffTint
        }
    }
}
@IBDesignable extension UIButton {
    
    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }
    
    
}
