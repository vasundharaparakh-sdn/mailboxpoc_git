//
//  FontExtension.swift
//  WoundCarePros
//
//  Created by Vasundhara Parakh on 7/1/19.
//  Copyright © 2019 Ratnesh Swarnkar. All rights reserved.
//

import UIKit

@objc extension UIFont {
    
    
    class func navigationTitleFont() -> UIFont {
        return UIFont(name: "Poppins-Medium", size: 20.0)!
    }
    
    //MARK:- Poppins SemiBold
    class func PoppinsSemiBold(fontSize : CGFloat) -> UIFont{
        return UIFont(name: "Poppins-SemiBold", size: fontSize)!
    }
    /*
    class func PoppinsSemiBold24() -> UIFont {
        return UIFont(name: "Poppins-SemiBold", size: 24.0)!
    }
    
    class func PoppinsSemiBold22() -> UIFont {
        return UIFont(name: "Poppins-SemiBold", size: 22.0)!
    }

    class func PoppinsSemiBold20() -> UIFont {
        return UIFont(name: "Poppins-SemiBold", size: 20.0)!
    }

    class func PoppinsSemiBold18() -> UIFont {
        return UIFont(name: "Poppins-SemiBold", size: 18.0)!
    }

    class func PoppinsSemiBold16() -> UIFont {
        return UIFont(name: "Poppins-SemiBold", size: 16.0)!
    }

    class func PoppinsSemiBold14() -> UIFont {
        return UIFont(name: "Poppins-SemiBold", size: 14.0)!
    }
    
    class func PoppinsSemiBold12() -> UIFont {
        return UIFont(name: "Poppins-SemiBold", size: 12.0)!
    }

    class func PoppinsSemiBold10() -> UIFont {
        return UIFont(name: "Poppins-SemiBold", size: 10.0)!
    }
    */
    //MARK:- Poppins Regular
    class func PoppinsRegular(fontSize : CGFloat) -> UIFont{
        return UIFont(name: "Poppins-Regular", size: fontSize)!
    }
    /*
    class func PoppinsRegular12() -> UIFont {
        return UIFont(name: "Poppins-Regular", size: 12.0)!
    }

    class func PoppinsRegular14() -> UIFont {
        return UIFont(name: "Poppins-Regular", size: 14.0)!
    }

    class func PoppinsRegular16() -> UIFont {
        return UIFont(name: "Poppins-Regular", size: 16.0)!
    }

    class func PoppinsRegular18() -> UIFont {
        return UIFont(name: "Poppins-Regular", size: 18.0)!
    }

    class func PoppinsRegular20() -> UIFont {
        return UIFont(name: "Poppins-Regular", size: 20.0)!
    }
    */
    //MARK:- Poppins Medium
    class func PoppinsMedium(fontSize : CGFloat) -> UIFont{
        return UIFont(name: "Poppins-Medium", size: fontSize)!
    }
    /*
    class func PoppinsMedium20() -> UIFont {
        return UIFont(name: "Poppins-Medium", size: 20.0)!
    }

    class func PoppinsMedium14() -> UIFont {
        return UIFont(name: "Poppins-Medium", size: 14.0)!
    }
    */
    //MARK:- Poppins Italics
    class func PoppinsItalic(fontSize : CGFloat) -> UIFont{
        return UIFont(name: "Poppins-Italic", size: fontSize)!
    }
    /*
    class func PoppinsItalic14() -> UIFont {
        return UIFont(name: "Poppins-Italic", size: 14.0)!
    }*/

}
