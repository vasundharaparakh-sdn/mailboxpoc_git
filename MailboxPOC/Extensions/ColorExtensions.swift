//
//  ColorExtensions.swift
//  neobook
//
//  Created by SDN MacMini 17 on 20/01/17.
//  Copyright © 2017 Mohit Choudhary. All rights reserved.
//

import UIKit
extension UIColor {
    class func appScreenBgColor() -> UIColor{
        return UIColor(red: 246.0/256.0, green: 246.0/256.0, blue: 249.0/256.0, alpha: 1.0)
    }
    class func navigationTitleColor() -> UIColor{
        return UIColor(red: 69.0/256.0, green: 79.0/256.0, blue: 99.0/256.0, alpha: 1.0)
    }
    
    class func appThemeColor() -> UIColor {
        return UIColor(red: 0.22, green: 0.58, blue: 0.29, alpha: 1.0)
    }
    
    class func appDarkGreyColor() -> UIColor {
        return UIColor(red: 69.0/256.0, green: 79.0/256.0, blue: 99.0/256.0, alpha: 1.0)
    }
    
    class func appGreyColor() -> UIColor{
        return UIColor(red: 149.0/256.0, green: 157.0/256.0, blue: 173.0/256.0, alpha: 1.0)
    }
 
    class func appRedColor() -> UIColor{
        return UIColor(red: 207.0/256.0, green: 23.0/256.0, blue: 31.0/256.0, alpha: 1.0)
    }
    
    class func appLightGreyColor() -> UIColor{
        return UIColor(red: 247.0/256.0, green: 247.0/256.0, blue: 250.0/256.0, alpha: 1.0)
    }

    class func appTableBackgroundColor() -> UIColor{
        return UIColor(red: 246.0/256.0, green: 246.0/256.0, blue: 249.0/256.0, alpha: 1.0)
    }

    class func appTableSelectionGreyColor() -> UIColor{
        return UIColor(red: 245.0/256.0, green: 246.0/256.0, blue: 249.0/256.0, alpha: 1.0)
    }
    
    class func transparentBackground() -> UIColor{
        return UIColor.black.withAlphaComponent(0.6)
    }
    
    class func graphStrokeColor() -> UIColor{
        return UIColor(red: 207.0/256.0, green: 23.0/256.0, blue: 31.0/256.0, alpha: 1.0)
    }

    class func graphFillColor() -> UIColor{
        return UIColor(red: 207.0/256.0, green: 23.0/256.0, blue: 31.0/256.0, alpha: 0.2)
    }
    
    class func anatomyBgColor() -> UIColor{
        return UIColor(red: 207.0/256.0, green: 23.0/256.0, blue: 31.0/256.0, alpha: 1.0)
    }
}
