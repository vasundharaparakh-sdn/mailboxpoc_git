//
//  ImageViewExtensions.swift
//  neobook
//
//  Created by SDN MacMini 17 on 27/01/17.
//  Copyright © 2017 Mohit Choudhary. All rights reserved.
//

import UIKit


extension UIImageView{
    func roundedCorner()->Void{
        self.layer.masksToBounds = true
        self.layer.cornerRadius = self.frame.size.width/2
    }
    
}
