//
//  ImagePickerViewController.swift
//  neobook
//
//  Created by SDN MacMini 17 on 25/01/17.
//  Copyright © 2017 Mohit Choudhary. All rights reserved.
//

import UIKit

import AVFoundation

protocol ImagePickerViewControllerDelegate{
    func getSelectedImage(image:UIImage, tag:NSInteger)
    func multiSelectedImage(images:[UIImage],tag:NSInteger)
    func removeImage(tag:NSInteger)
}

class ImagePickerViewController: UIImagePickerController ,UIImagePickerControllerDelegate  {
    
    var imagePickerViewControllerDelegate:ImagePickerViewControllerDelegate?
    var receivedTag:NSInteger?
    var croppedFrame:CGRect? = nil
    var newAngle:NSInteger? = 0
    var imageSelectionCount:NSInteger?
    var currentImage:UIImage? = nil
    var isRequireToCropImage:Bool?
    var isMultiselection:Bool=false
    var isProfilePic = false
    var currentViewController:UIViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showOptionAlert(viewController: BaseViewControler,receivedDelegate:ImagePickerViewControllerDelegate?, tag:NSInteger, isRequireToRemoveImage:Bool, sender:UIButton, complition:@escaping (String?) -> Void) {
        if isProfilePic{
            let alertController = UIAlertController(title: AppDetails.applictionName, message:
                Macros.CHOOSE_OPTIONS, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: Macros.Camera, style: UIAlertAction.Style.default,handler: { (alertController) in
                self.authorizeAndOpenCamera(sender: sender, viewController: viewController, complition: { (status) in
                    complition(status)
                })
                
            }))
            alertController.addAction(UIAlertAction(title: Macros.PHOTO_LIBRARY, style: UIAlertAction.Style.default,handler: { (alertController) in
                self.openPhotoLibrary(sender: sender,viewController: viewController)
                
            }))
            
            alertController.addAction(UIAlertAction(title: Macros.CANCEL, style: UIAlertAction.Style.cancel, handler: { (alertController) in
                complition(nil)
            }))
            
            currentViewController = viewController
            if (UIDevice.current.userInterfaceIdiom == .pad){
                if alertController.responds(to: #selector(getter: viewController.popoverPresentationController)) {
                    alertController.popoverPresentationController?.sourceView = viewController.view
                    
                    alertController.popoverPresentationController?.sourceRect = CGRect(x: 0, y: UIScreen.main.bounds.size.height, width: UIScreen.main.bounds.size.width, height: 320)
                    viewController.present(alertController, animated: true, completion: nil)
                }
            }else{
                alertController.modalPresentationStyle = .popover
                viewController.present(alertController, animated: true, completion: nil)
            }
            complition("success")
        }else{
            self.authorizeAndOpenCamera(sender: sender, viewController: viewController, complition: { (status) in
                complition(status)
            })
        }
    }
    
    func authorizeAndOpenCamera(sender: UIButton,viewController: BaseViewControler,complition:@escaping (String?) -> Void){
        let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        switch authStatus {
        case .authorized:
            if !PlatformUtils.isSimulator{
                self.openCamera(sender: sender, viewController: viewController)
            }
        case .denied:
            complition("denied")
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video, completionHandler: {accessGranted in
                guard accessGranted == true else { return }
                self.openCamera(sender: sender, viewController: viewController)
            })
        case .restricted:
            complition("restricted")
        }

    }
    
    func openPhotoLibrary(sender: UIButton, viewController:UIViewController) {
        self.sourceType = .photoLibrary
        self.allowsEditing = true
        self.modalPresentationStyle = .popover
        let presentationController = self.popoverPresentationController
        // You must set a sourceView or barButtonItem so that it can
        // draw a "bubble" with an arrow pointing at the right thing.
        presentationController?.sourceView = sender
        
        
        if (UIDevice.current.userInterfaceIdiom == .pad){
            if self.responds(to: #selector(getter: viewController.popoverPresentationController)) {
                self.popoverPresentationController?.sourceView = viewController.view
                
                self.popoverPresentationController?.sourceRect = CGRect(x: 0, y: UIScreen.main.bounds.size.height, width: UIScreen.main.bounds.size.width, height: 320)
                viewController.present(self, animated: true, completion: nil)
            }
        }else{
            self.modalPresentationStyle = .popover
            viewController.present(self, animated: true, completion: nil)
        }
        
      //  viewController.present(self, animated: true) {}
    }
    
   
    func openCamera(sender: UIButton, viewController:UIViewController) {
        self.sourceType = .camera
        if (UIDevice.current.userInterfaceIdiom == .pad){
            if self.responds(to: #selector(getter: viewController.popoverPresentationController)) {
                self.popoverPresentationController?.sourceView = viewController.view
                self.popoverPresentationController?.sourceRect = CGRect(x: 0, y: UIScreen.main.bounds.size.height, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
                viewController.present(self, animated: true, completion: nil)
            }
        }else{
            self.modalPresentationStyle = .popover
            viewController.present(self, animated: true, completion: nil)
        }
        
       // viewController.present(self, animated: true, completion: nil)
    }
    
    
    func openSavedPhotosAlbum(sender: UIButton, viewController:UIViewController) {
        self.sourceType = .savedPhotosAlbum
        viewController.present(self, animated: true, completion: nil)
    }
    
    
    // MARK: - UIImagePickerControllerDelegate Methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
       
        if let chosenImage = info[.originalImage] as? UIImage {
         
            imagePickerViewControllerDelegate?.getSelectedImage(image: chosenImage,tag: receivedTag!)
            
        } else{
            print("Something went wrong")
        }
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
}



