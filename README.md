# MailboxPOC_Git

The sample code includes following features - 
1. List of Inbox,Sent,Deleted Items with pagination
2. Compose Message
3. Delete - Inbox,Sent,Deleted
4. Message Detail
5. Thread Message Detail
6. Compose Message has a limit to attach maximum 5 files only. File type can be image or pdf. 
Attachment source can be photo gallery or iCloud.
7. Attachment preview in message & thread detail.